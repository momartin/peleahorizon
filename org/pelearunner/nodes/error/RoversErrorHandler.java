/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.pelearunner.nodes.error;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import org.jdom.JDOMException;
import org.pelea.core.module.basic.error.ErrorHandler;
import org.pelea.languages.pddl.structures.NodeList;
import org.pelea.languages.pddl.structures.nodes.Node;
import org.pelea.languages.pddl.structures.nodes.Predicate;

/**
 *
 * @author moises
 */
public class RoversErrorHandler extends ErrorHandler {

    @Override
    public ArrayList generateDataError(NodeList predicates, String action) throws JDOMException, IOException{
       
       int i;
       int position = 0;
       
       ArrayList<String> resultData = new ArrayList();
       String values[] = this.getActionValues(action);
        
       if (values[0].toUpperCase().contains("NAVIGATE")) {
            ArrayList<Node> candidates = (ArrayList<Node>) predicates.getNodesByNameAndValue("can_traverse", new String[]{values[1], values[2]});
           
            i = 0;
           
            while (i < candidates.size()) {
               if (((Predicate) candidates.get(i)).getValue(2).toUpperCase().matches(values[3].toUpperCase()))
                    candidates.remove(i);
               else
                    i++;
            }
            
            if (candidates.size() > 0) {
               position = (candidates.size() > 1) ? (new Random()).nextInt(candidates.size()):0;
               resultData.add(((Predicate) candidates.get(position)).getValue(2));
            }
            else { //original waypoint
                resultData.add(values[3].toLowerCase());
            }
           
            candidates = (ArrayList<Node>) predicates.getNodesByNameAndValue("can_traverse", new String[]{values[1], values[3]});
            
            i = 0;
           
            while (i < candidates.size()) {
                if (((Predicate) candidates.get(i)).getValue(2).toUpperCase().matches(values[2].toUpperCase()))
                    candidates.remove(i);
                else
                    i++;
            }
            
            if (candidates.size() > 0) {
                position = (candidates.size() > 1) ? (new Random()).nextInt(candidates.size()):0;
                resultData.add(((Predicate) candidates.get(position)).getValue(2));
            }
            else { //original waypoint
              resultData.add(values[2].toLowerCase());
            }
        }
        return resultData;
    }
}
