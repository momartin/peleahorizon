package org.pelearunner.nodes;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.rmi.*;
import java.util.ArrayList;
import org.jdom.JDOMException;
import org.pelea.core.communication.*;
import org.pelea.core.communication.connector.RMICommunicationModel;
import org.pelea.core.configuration.configuration;
import org.pelea.core.module.Module;
import org.pelea.languages.pddl.plan.ActionPlan;
import org.pelea.monitoring.info.PDDL.InfoPDDL;
import org.pelea.response.Response;
import org.pelea.utils.Util;
import org.pelea.utils.experimenter.Experiment;
import org.pelea.wrappers.Monitoring;

/**
 *
 * @author moises
 */
public class MonitoringHorizon extends Monitoring
{
    private final boolean useHorizon;
    private int numProblems;
    private int horizon;
    private int k;
    private String[] horizons;
    
    public MonitoringHorizon(String name) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NotBoundException, MalformedURLException, RemoteException, Exception {

        super(name, Module.RMI);

        this.numProblems = configuration.getInstance().getParameter("GENERAL", "PROBLEM").split(",").length;
        this.useHorizon  = Boolean.parseBoolean(configuration.getInstance().getParameter("GENERAL", "USE_HORIZON"));
        
        this.data.addVariable("horizon", "100000");
       
        if (useHorizon) {
            this.horizons = configuration.getInstance().getParameter("GENERAL", "HORIZON").split(",");
            this.rounds  = Integer.parseInt(configuration.getInstance().getParameter("GENERAL", "ROUNDS")) * this.horizons.length * numProblems;
            this.horizon = 0;
        }
        else {
            this.rounds = Integer.parseInt(configuration.getInstance().getParameter("GENERAL", "ROUNDS")) * numProblems;
        }
    }
    
    public MonitoringHorizon(String name, String masterPID) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NotBoundException, MalformedURLException, RemoteException, Exception {
        
        super(name, Module.RMI, masterPID);

        this.numProblems = configuration.getInstance().getParameter("GENERAL", "PROBLEM").split(",").length;
        this.useHorizon  = Boolean.parseBoolean(configuration.getInstance().getParameter("GENERAL", "USE_HORIZON"));
        
        this.data.addVariable("horizon", "100000");
       
        if (useHorizon) {
            this.horizons = configuration.getInstance().getParameter("GENERAL", "HORIZON").split(",");
            this.rounds  = Integer.parseInt(configuration.getInstance().getParameter("GENERAL", "ROUNDS")) * this.horizons.length * numProblems;
            this.horizon = 0;
        }
        else {
            this.rounds = Integer.parseInt(configuration.getInstance().getParameter("GENERAL", "ROUNDS")) * numProblems;
        }    
    }
    
    @Override
    protected void solveProblem(Message msg) throws RemoteException, IOException, JDOMException {
        if ((this.round > 0) && (this.round % (this.rounds / horizons.length) == 0)) {
            this.horizon++;
        }
        
        Experiment experiment = new Experiment();
       
        if (this.useHorizon) {
            experiment.addValue("Horizon", String.valueOf(this.horizons[this.horizon]));
        }
        
        this.info.addExperiment(experiment);

        Response res = new Response(msg.getContent());
        
        this.info.resetPlan(); //Remove last plan
        this.info.loadState(res.getNode("problem", false));
        this.data.setStateH(this.info.getProblemXML());
        this.info.loadDomain(res.getNode("domain", false));
        //this.data.setDomainH(this.info.getDomainXML());
        
        this.data.updateVariable("horizon", this.horizons[this.horizon]);
        Message send = new Message(this.commumicationModel.getType(), Messages.NODE_DECISSIONSUPPORT, Messages.MSG_GETPLANINFO, this.data.buildXML(true, false, true, false, false, false, false, new String[]{"horizon"}));
        ((RMICommunicationModel) this.commumicationModel).sendMessage(send);
    }

    @Override
    protected void processState(Message msg) throws RemoteException, IOException, JDOMException, Exception {
        Response res = new Response(msg.getContent());

        if (this.info.isPlan())
        {
            this.info.generateNextState();
            
            //Compare new state for environment with the newState generated previosly.
            if ((this.info.isValidState(res.getNode("state", false), true, true, this.validateState)))
            {
                this.k++;
                this.info.executeAction();

                //Save new state as current state
                this.data.setStateH(this.info.getProblemXML());

                if (this.info.moreActions()) {
                    this.executeAction();
                }
                else {
                    if (this.info.goalsReached()){
                        Util.printDebug(this.getName(), "Problem Solved");
                        this.info.finishExperiment(Experiment.SOLVED);
                        this.finishRound();
                    }
                    else {
                        
                        Util.printDebug(this.getName(), "Replanning 2");

                        this.data.setStateH(this.info.getProblemXML());
                        ((InfoPDDL) this.info).saveProblem(this.outputDir, "problem" + this.info.getReplanningSteps() + ".pddl");

                        //Reset expected answers
                        this.answers = 0;
                        this.k = 0;

                        //Delete wrong actions
                        this.info.deletePlan();

                        //Generate a new plan of actions
                        this.data.updateVariable("horizon", this.horizons[this.horizon]);
                        Message send = new Message(this.commumicationModel.getType(), Messages.NODE_DECISSIONSUPPORT, Messages.MSG_REPAIRORPEPLAN, this.data.buildXML(true, false, true, false, true, false, false, new String[]{"horizon"}));
                        ((RMICommunicationModel) this.commumicationModel).sendMessage(send);
                    }
                }
            }
            else {
                
                //Load real state
               this.info.loadState(res.getNode("state", false));
               this.data.setStateH(this.info.getProblemXML());
               
               ((InfoPDDL) this.info).saveProblem(this.outputDir, "problem" + this.info.getReplanningSteps() + ".pddl");

               Util.printDebug(this.getName(), "Replanning");

               //Reset expected answers
               this.answers = 0;
               this.k = 0;

               //Delete wrong actions
               this.info.deletePlan();

               //Generate a new plan of actions
               this.data.updateVariable("horizon", this.horizons[this.horizon]);
               Message send = new Message(this.commumicationModel.getType(), Messages.NODE_DECISSIONSUPPORT, Messages.MSG_REPAIRORPEPLAN, this.data.buildXML(true, false, true, false, true, false, false, new String[]{"horizon"}));
               ((RMICommunicationModel) this.commumicationModel).sendMessage(send);
            }
        }
        else
        { 
            //Initial state sended by execution nodes, it is loaded as initial state.
            this.info.loadState(res.getNode("state", false));
                
            this.data.updateVariable("horizon", this.horizons[this.horizon]);
            Message send = new Message(this.commumicationModel.getType(), Messages.NODE_DECISSIONSUPPORT, Messages.MSG_GETPLANINFO, this.data.buildXML(true, false, true, false, false, false, false, new String[]{"horizon"}));
            /*this.answers = */((RMICommunicationModel) this.commumicationModel).sendMessage(send);
        }
    }
    
    @Override
    protected boolean executeAction() throws RemoteException {                              
        //ArrayList references    = (ArrayList) this.me.getReferenceListByType(messages.NODE_EXECUTION);
        ActionPlan ap = this.info.getNextAction();
        Message send = null;
        
        for (int i = 0; i < ap.size(); i++) {
            if (this.pddlIden) {
                ArrayList<String> idems = ap.get(i).getValuesByType(this.code);
                send = new Message(this.commumicationModel.getType(), Messages.NODE_EXECUTION, Messages.MSG_EXECUTEPLAN, ap.getXmlByPosition(i));
                
                this.answers += this.answers = ((RMICommunicationModel) this.commumicationModel).sendMessage(send, idems);
            }
            else {
                send = new Message(this.commumicationModel.getType(), Messages.NODE_EXECUTION, Messages.MSG_EXECUTEPLAN, ap.getXmlByPosition(i));
                this.answers += this.answers = ((RMICommunicationModel) this.commumicationModel).sendMessage(send);
            }
        }
            
        return true;                                       
    } 
    
    @Override
    protected void processPlan(Message msg) throws RemoteException, JDOMException, IOException, Exception {
        
        Response res = new Response(msg.getContent());
        
        this.k = 0;
        
        //Loading plan of actions in info structure to analyze the execution of the accions
        this.info.loadPlan(res.getNode("plans", false));
        //Saving plan data in temporal buffer
        this.data.setPlanH(res.getNode("plans", false));

        if (this.info.moreActions()) {
            this.executeAction();
        }
        else {
            Util.printDebug(this.getName(), "No more actions");
            this.info.finishExperiment(Experiment.DEADEND);
            this.finishRound();
        }
    }

    @Override
    protected void translateState(Message msg) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}


