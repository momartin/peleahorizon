/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.pelearunner.nodes;

import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.pelea.core.communication.Message;
import org.pelea.core.communication.Messages;
import org.pelea.core.module.Module;
import org.pelea.wrappers.DecisionSupport;
import org.pelea.planner.Planner;
import org.pelea.planners.AbstractFD;
import org.pelea.response.Response;
import org.pelea.utils.Util;

/**
 *
 * @author moises
 */
public class DecissionSupportHorizon extends DecisionSupport {
    
    public DecissionSupportHorizon(String name) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NotBoundException, MalformedURLException, RemoteException {
        super(name, Module.RMI);
    }

    @Override
    public String RepairOrReplan(String stateH, String domainH, String planH) {
        String plan     = "";
        int position    = 0;
        
        try 
        {    
            position    = this.getPlanner(Planner.MODE_REPLANING);
            plan        = this.planners.get(position).getRePlanH(domainH, stateH, planH);
            
            return plan;
        
        } 
        catch (Exception ex) 
        {
            Util.printError(this.getName(), "Generating plan of actions (" + ex.toString() + ")");
            
            plan += "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
            plan += "<define xmlns=\"http://www.pelea.org/xPddl\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.pelea.org/xPddl xPddl.xsd\">";
            plan += "<error>";
            plan += "Replanning using planner " + this.planners.get(position).getName();
            plan += "</error>";
            plan += "</define>";
            
            return plan;
        }
    }

    @Override
    public String getPlanHInfoMonitor(String stateH, String domainH) {
        String plan     = "";
        int position    = 0;
        
        try 
        {    
            position    = this.getPlanner(Planner.MODE_PLANING);
            plan = this.planners.get(position).getPlanH(domainH, stateH);
            
            return plan;
        } 
        catch (Exception ex) 
        {
            Util.printError(this.getName(), "Generating plan of actions (" + ex.toString() + ")");
            
            plan += "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
            plan += "<define xmlns=\"http://www.pelea.org/xPddl\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.pelea.org/xPddl xPddl.xsd\">";
            plan += "<error>";
            plan += "Generating plan of actions using planner " + this.planners.get(position).getName();
            plan += "</error>";
            plan += "</define>";
            
            return plan;
        }
    }

    @Override
    public Message messageHandler(Message recieve) throws Exception {
        
        Response res;
        
        switch (recieve.getTypeMsg()) {
            case Messages.MSG_START:
                this.state = Module.RUNNING;
                return null;
            case Messages.MSG_GETPLANINFO:
                if (this.isRunning()) {
                    res = new Response(recieve.getContent());
                    this.setHorizon(res.getNode("horizon", false));
                    return new Message(this.commumicationModel.getType(), Messages.NODE_MONITORING, Messages.MSG_GETPLANINFO_RES, this.getPlanHInfoMonitor(res.getNode("problem", true), res.getNode("domain", true)));
                }
                break;
            case Messages.MSG_REPAIRORPEPLAN:
                if (this.isRunning()) { 
                    res = new Response(recieve.getContent());
                    this.setHorizon(res.getNode("horizon", false));
                    return new Message(this.commumicationModel.getType(), Messages.NODE_MONITORING, Messages.MSG_REPAIRORPEPLAN_RES, this.RepairOrReplan(res.getNode("problem", true), res.getNode("domain", true), res.getNode("plans", true)));
                }
                break;
            case Messages.MSG_STOP:
                if (this.isRunning()) this.state = Module.STOPPING;
                return null;
        }
        
        return null;
    }

    @Override
    public void errorHandler() throws Exception {
    }
    
    private void setHorizon(String value) throws JDOMException, IOException {
        
        XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
        SAXBuilder builder = new SAXBuilder();
        Document document = (Document) builder.build(new StringReader(value));        
        String horizon = document.getRootElement().getText();
        
        for (int i = 0; i < this.planners.size(); i++) {
            
            if (this.planners.get(i) instanceof AbstractFD) {
                ((AbstractFD) this.planners.get(i)).setHorizon(horizon);
            }
        }
    }
}
